/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nic.mobitax;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author nyakundid
 */
@RestController
public class ValidateEslipController {

    //http://10.4.115.184:8088/kra/ccrs/checkEslip/eslipNumber/2017JKA3290150E
    @RequestMapping(value = "/validateEslip", method = RequestMethod.GET)
    public String validateEslip() {

        String response = null;

        
        return response;
        
    }
    
    /*
        @Expected response
            <?xml version="1.0" encoding="UTF-8"?>
            <ESLIP>
              <ESLIPHEADER>
                <SystemCode>SIMBA</SystemCode>
                <EslipNumber>2017JKA3290150E</EslipNumber>
                <SlipPaymentCode>SIMBA</SlipPaymentCode>
                <PaymentAdviceDate>2017-01-07T10:00:09</PaymentAdviceDate>
                <TaxpayerPin>P051124154Q</TaxpayerPin>
                <TaxpayerFullName>FREIGHT IN TIME LIMITED</TaxpayerFullName>
                <DepartmentCode>CSD</DepartmentCode>
                <RegionCode>CR</RegionCode>
                <StationCode>JKA</StationCode>
                <TotalAmount>49109</TotalAmount>
                <DocRefNumber>2017JKA3290150E</DocRefNumber>
                <Currency>KES</Currency>
              </ESLIPHEADER>
              <ESLIPDETAILS>
                <TaxCode>1518</TaxCode>
                <TaxHead>FEES</TaxHead>
                <TaxComponent>CONCESSION FEES</TaxComponent>
                <AmountPerTax>250</AmountPerTax>
                <TaxPeriod>2017-01-07</TaxPeriod>
              </ESLIPDETAILS>
              <ESLIPDETAILS>
                <TaxCode>1002</TaxCode>
                <TaxHead>IMPORT DUTY</TaxHead>
                <TaxComponent>IMPORT DUTY</TaxComponent>
                <AmountPerTax>27144</AmountPerTax>
                <TaxPeriod>2017-01-07</TaxPeriod>
              </ESLIPDETAILS>
              <ESLIPDETAILS>
                <TaxCode>1202</TaxCode>
                <TaxHead>VAT IMPORTS</TaxHead>
                <TaxComponent>VAT IMPORTS</TaxComponent>
                <AmountPerTax>21715</AmountPerTax>
                <TaxPeriod>2017-01-07</TaxPeriod>
              </ESLIPDETAILS>
            </ESLIP>


    
    */

}
